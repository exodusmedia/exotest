<?php
/**
 * ExoTest Reflective Suite
 * Test the ExoTest Framework is operating correctly
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoTest\SelfTest;
class Suite extends \ExoTest\Suite
{
	public function test_assert()
	{
		// check a TRUE assertion works
		$count = count($this->results);
		$this->assert(TRUE);
		$result_created = count($this->results) - $count == 1;
		$result = end($this->results);
		$result_passed = $result->result == self::RESULT_PASS;
		$this->rewind();
		$this->assert($result_created);

		$this->assert($result_passed);
		
		// check a FALSE assertion fails
		$count = count($this->results);
		$this->assert(FALSE);
		$result_created = count($this->results) - $count == 1;
		$result = end($this->results);
		$result_failed = $result->result == self::RESULT_FAIL;
		$description_notempty = !empty($result->description);
		$this->rewind();
		$this->assert($result_created);
		$this->assert($description_notempty);
		$this->assert($result_failed);
	}

	public function test_assert_false()
	{
		// check a FALSE assertion works
		$count = count($this->results);
		$this->assert_false(FALSE);
		$result_created = count($this->results) - $count == 1;
		$result = end($this->results);
		$result_passed = $result->result == self::RESULT_PASS;
		$this->rewind();
		$this->assert($result_created);
		$this->assert($result_passed);
		
		// check a TRUE assertion fails
		$count = count($this->results);
		$this->assert_false(TRUE);
		$result_created = count($this->results) - $count == 1;
		$result = end($this->results);
		$result_failed = $result->result == self::RESULT_FAIL;
		$this->rewind();
		$this->assert($result_created);
		$this->assert($result_failed);
	}

	public function test_assert_equals()
	{
		// check an equal assertion works
		$count = count($this->results);
		$this->assert_equals('a', 'a');
		$result_created = count($this->results) - $count == 1;
		$result = end($this->results);
		$result_passed = $result->result == self::RESULT_PASS;
		$this->rewind();
		$this->assert($result_created);
		$this->assert($result_passed);
		
		// check an inequal assertion fails
		$count = count($this->results);
		$this->assert_equals('a', 'b');
		$result_created = count($this->results) - $count == 1;
		$result = end($this->results);
		$result_failed = $result->result == self::RESULT_FAIL;
		$this->rewind();
		$this->assert($result_created);
		$this->assert($result_failed);
	}

	public function test_rewind()
	{
		// rewind 1
		$count = count($this->results);
		$this->assert(TRUE);
		$result_created = count($this->results) - $count == 1;
		$this->rewind();
		$result_rewound = count($this->results) - $count == 0;
		$this->assert($result_created);
		$this->assert($result_rewound);

		// rewind two
		$count = count($this->results);
		$this->assert(TRUE);
		$this->assert(TRUE);
		$results_created = count($this->results) - $count == 2;
		$this->rewind(2);
		$results_rewound = count($this->results) - $count == 0;
		$this->assert($results_created);
		$this->assert($results_rewound);

		// test that the rewinds are valid values
		$this->assert(TRUE);
		$assert = $this->rewind();
		$this->assert($assert->result == self::RESULT_PASS);

		$this->assert(FALSE);
		$this->assert(TRUE);
		$asserts = $this->rewind(2);
		// in reverse order!
		$this->assert($asserts[0]->result == self::RESULT_PASS);
		$this->assert($asserts[1]->result == self::RESULT_FAIL);
	}

	/**
	 * Test that assert_contains works properly
	 */
	public function test_assert_contains()
	{
		$array = array('name' => 'Guillaume VanderEst', 'email' => 'guillaume@vanderest.org');
		$object = (object)$array;

		$this->assert_contains(TRUE, TRUE);
		$this->assert_contains('hello', 'e');
		$this->assert_contains(1234, '23');

		$this->assertContains($array, $array);
		$this->assertContains($object, $object);
		$this->assertContains($array, $object);
		$this->assertContains($object, $array);
		$this->assertContains($array, 'Guillaume VanderEst');

		$this->assertContains('Tyler', 'z');
		$assert = $this->rewind();
		$this->assertFailed($assert);
	}

	/**
	 * Test assert_array
	 */
	public function test_assert_array()
	{
		$array = array();
		$this->assertArray($array);
	}
	
	/**
	 * Test assert_not_array
	 */
	public function test_assert_not_array()
	{
		$this->assertNotArray(123);
	}

	/**
	 * Test Undefined Exception Type
	 * @exception
	 */
	public function test_undefined_exception()
	{
		throw new Exception('Success');
	}

	/**
	 * Test Exception handling
	 * @exception ExoTest\SelfTest\Exception
	 */
	public function test_exception()
	{
		throw new Exception('Success');
	}

	/**
	 * Test base exception
	 * @exception Exception
	 */
	public function test_base_exception()
	{
		throw new \Exception('Success');
	}

	/**
	 * Force a failure
	 */
	public function test_natural_failure()
	{
		$this->assert(FALSE);
		$assert = $this->rewind();
		$this->assert($assert->result == self::RESULT_FAIL);
	}

	/**
	 * @fixme Add this functionality
	 */
	public function test_assert_passed()
	{
	}

	/**
	 * @fixme Add this functionality
	 */
	public function test_assert_failed()
	{
	}
}
