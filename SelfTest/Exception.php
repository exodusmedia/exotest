<?php
/**
 * ExoTest Self-Test Exception
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoTest\SelfTest;
class Exception extends \Exception {}
