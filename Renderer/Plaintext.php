<?php
/**
 * ExoTest HTML Renderer
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoTest\Renderer;
use ExoTest\Suite;
class Plaintext
{
	public function display_results($results)
	{
		ob_start();

$tests = array();

$test_count = count($tests);
$assert_count = count($results);
$pass_count = 0;
$fail_count = 0;
$skip_count = 0;
foreach ($results as $result)
{
	if (!in_array($result->name, $tests))
	{
		$tests[] = $result->name;
	}

	if ($result->result == Suite::RESULT_FAIL)
	{
		$fail_count++;
		continue;
	}

	if ($result->result == Suite::RESULT_SKIP)
	{
		$skip_count++;
		continue;
	}

	$pass_count++;
}
		?>

Execution of <?= get_class($this->suite) ?>

<?php if ($fail_count > 0): ?>
<?php if (abs($fail_count) == 1): ?>
There was <?= $fail_count ?> failure:
<?php else: ?>
There were <?= $fail_count ?> failures:
<?php endif; ?>
<?php $count = 0; ?>
<?php foreach ($this->results as $result): ?>

<?php if ($result->result == self::RESULT_FAIL): ?>
<?= ++$count ?>) <?= $result->method ?>

<?= $result->description ?>

<?= $result->file ?>:<?= $result->line ?>
<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>

Tests: <?= $test_count ?>, Assertions: <?= $assert_count ?>, Failures: <?= $fail_count ?>, Skipped: <?= $skip_count ?>.

<?php
		return ob_get_clean();
	}
}
