<?php
/**
 * JSON Encode
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoTest\Renderer;
class JSON
{
	public function display_results($results)
	{
		return json_encode($results);
	}
}
