<?php
/**
 * ExoTest HTML Renderer
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoTest\Renderer;
use ExoTest\Suite;
class HTML extends \ExoTest\Renderer\Plaintext
{
	const CODE_BUFFER = 3;

	public function ellipsis($string, $length = 50, $symbol = '..')
	{
		$length -= strlen($symbol);
		if (strlen($string) > $length)
		{
			return substr($string, 0, $length) . $symbol;
		}
		return $string;
	}
	public function display_results($results)
	{
		$passes = 0;
		$fails = 0;
		$skips = 0;
		$asserts = 0;

		$tests = array();
		$test = NULL;
		$last_name = NULL;
		foreach ($results as $result)
		{
			if ($result->name !== $last_name)
			{
				if ($test !== NULL)
				{
					$tests[] = $test;
				}
				$test = (object)array(
					'name' => $result->name,
					'time' => 0,
					'passes' => 0,
					'fails' => 0,
					'skips' => 0,
					'asserts' => array()
				);
				$last_name = $result->name;
			}

			$test->time += $result->time;
			$test->asserts++;
			$asserts++;

			switch ($result->result)
			{
				case Suite::RESULT_PASS: $passes++; $test->passes++; break;
				case Suite::RESULT_SKIP: $skips++; $test->skips++; break;
				default: $fails++; $test->fails++; break;
			}

			$test->asserts[] = $result;
		}
		$tests[] = $test;

		ob_start();
		?>
<div class="exotest">
	<h1><?= get_class($this->suite) ?></h1>
	<p class="summary"><?= sprintf("%d Tests, %d Asserts, %d Failed, %d Skips in %s seconds", 
		count($tests),
		$asserts,
		$fails,
		$skips,
		$test->time
	) ?></p>
	<table class="results">
		<thead class="header">
			<tr>
				<th class="count">#</th>
				<th class="start">Start</th>
				<th class="time">Time (ms)</th>
				<th class="result">Result</th>
				<th class="line">Line</th>
				<th class="description">Notes</th>
			</tr>
		</thead>
		<tbody>
			<?php $count = 0; ?>
			<?php $last_name = NULL; ?>
			<?php foreach ($tests as $test): ?>
				<tr class="grouping">
					<th class="name" colspan="6">
						<span class="name"><?= $test->name ?></span> 
						<span class="summary">(<?= count($test->asserts) ?> asserts, <?= $test->fails ?> failed, <?= $test->skips ?> skipped)</span>
					</th>
				</tr> <!-- .grouping -->
				<?php foreach ($test->asserts as $assert): ?>
					<?php
					// if the assert failed, output the code that caused the failure
					$code = NULL;
					if ($assert->result == Suite::RESULT_FAIL)
					{
						$lines = file($assert->file);
						$code .= "<div class=\"code-lines\">";
						$num_length = strlen($assert->line + self::CODE_BUFFER);
						$code .= '<div class="filename">' . $assert->file . '</div>';
						for ($x = max(0, $assert->line - self::CODE_BUFFER); $x < $assert->line + self::CODE_BUFFER; $x++)
						{
							$line = @$lines[$x];
							if ($line)
							{
								$classes = array('line');
								if ($x == $assert->line) { $classes[] = 'highlight'; }
								$code .= '<div class="' . implode(' ', $classes) . '">';
								$code .= str_pad($x, $num_length, '0', STR_PAD_LEFT) . ': ';
								$code .= htmlentities($line);
								$code .= '</div>';
							}
						}
						$code .= '</div>';
					}
					?>
					<tr class="result">
						<td class="count"><?= ++$count ?></td>
						<td class="start"><?= date('H:i:s', $assert->start) ?></td>
						<td class="time"><?= number_format($assert->time * 1000, 3) ?></td>
						<td class="result <?= $assert->result ?>"><?= $assert->result ?></td>
						<td class="line"><?= $assert->line ?></td>
						<td class="description"><?= htmlentities($this->ellipsis($assert->description, 200)) ?><?= $code ?></td>
					</tr> <!-- .result -->
				<?php endforeach; ?>
			<?php endforeach; ?>
		</tbody>
	</table> <!-- .results -->
</div> <!-- .exotest -->
<style>
.exotest .results {
	border: 4px solid #ccc;
	border-collapse: collapse;
}
.exotest th, .exotest td {
	text-align: left;
	padding: 4px 6px;
	font-family: monospace;
	border: 1px solid #bbb;
	vertical-align: top;
}
.exotest .grouping th {
	background: #eee;
}
.exotest th {
	background: #ccc;
	font-weight: bold;
}
.exotest .result .result, .exotest .result .time {
	text-align: center;
	text-transform: uppercase;
}
.exotest .result.fail, .exotest .code-lines .line.highlight {
	background: #fee !important;
	color: #800;
	border: 1px solid #abb;
}
.exotest .result.skip {
	background: #ffd;
	color: #a90;
}
.exotest .result.pass {
	background: #efe;
	color: #080;
}

.exotest .count {
	min-width: 30px;
	text-align: right;
}

.exotest .description {
	min-width: 300px;
}

.exotest .results tr:hover td {
	background: #fafafa;
}

.exotest .summary {
	color: #888;
}

.exotest .results tr:hover .result.fail {
	background: #edd;
}
.exotest .results tr:hover .result.pass {
	background: #ded;
}
.exotest .results tr:hover .result.skip {
	background: #ded;
}
.exotest .code-lines {
	margin-top: 15px;
	white-space: pre;
	border: 1px solid #eee;
}
.exotest .code-lines .line, .exotest .code-lines .filename {
	padding: 2px;
	line-height: 1.5;
}
.exotest .code-lines .filename {
	font-weight: bold;
	background: #ddd;
}
.exotest .code-lines .line:nth-child(2n) {
	background: #eee;
}
</style>
		<?php
		return ob_get_clean();
	}
}
