<?php
/**
 * ExoTest\Suite
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoTest;
use ReflectionClass;
use ReflectionMethod;
class Suite
{
	const RESULT_FAIL = 'fail';
	const RESULT_PASS = 'pass';
	const RESULT_SKIP = 'skip';

	static $class_files = array();
	protected $results = array();
	protected $options = array();
	protected $start_time;

	public function __construct($options = array())
	{
		if (empty($options['renderer'])) { unset($options['renderer']); }

		$this->tick();
		$this->options = array_merge(array(
			'renderer' => defined('STDIN') ? 'Plaintext' : 'HTML'
		), $options);
	}

	public static function get_class_filename($class)
	{
		if (empty($class)) { return NULL; }

		if (!array_key_exists($class, self::$class_files))
		{
			$reflect = new ReflectionClass($class);
			$filename = $reflect->getFileName();
			self::$class_files[$class] = $filename;
		}

		return self::$class_files[$class];
	}

	public static function get_class_tests($class)
	{
		$all_methods = get_class_methods($class);
		$tests = $all_methods;

		$system_tests = get_class_methods('ExoTest\\Suite');
		foreach ($tests as $index => $method)
		{
			// remove any "system" methods
			if (in_array($method, $system_tests))
			{
				unset($tests[$index]);
				continue;
			}

			// remove any non-public methods
			$reflect = new ReflectionMethod($class, $method);
			if (!$reflect->isPublic())
			{
				unset($tests[$index]);
				continue;
			}
		}
		return $tests;
	}

	public static function get_test_name($method)
	{
		return ucwords(preg_replace('#[^a-z0-9]+#i', ' ', $method));
	}

	/**
	 * Execute all, or defined, tests
	 * @param mixed $tests (optional) if array, execute functions in list, otherwise NULL runs all
	 * @return array results
	 */
	public function execute($tests = NULL)
	{
		// before, settings
		$display_errors = ini_get('display_errors');
		$error_reporting = ini_get('error_reporting');
		
		// temporary settings
		ini_set('display_errors', TRUE);
		ini_set('error_reporting', E_ALL | E_NOTICE);

		$this->tick();

		$class_tests = self::get_class_tests($this);
		if ($tests === NULL)
		{
			$tests = $class_tests;
		} elseif (is_scalar($tests)) {
			$tests = array($tests);
		}

		// error out on requesting tests that don't exist
		foreach ($tests as $method)
		{
			if (!in_array($method, $class_tests))
			{
				throw new Exception(sprintf('Test "%s" is not a valid test in suite "%s"', $method, get_class($this)));
				continue;
			}
		}

		// run all tests specified
		foreach ($tests as $test)
		{
			try
			{
				$this->test_name = $test;
				$this->assert_fixme($this, $test);
				$this->$test();
			// exceptions are tested as well, in case test expects them
			} catch (\Exception $e) {
				$this->assert_exception($e, $test);
			}
		}

		// revert settings
		ini_set('display_errors', $display_errors);
		ini_set('error_reporting', $error_reporting);
	}

	public function add_result($bool, $data = array())
	{
		// get the current time, to compare against suite start_time
		$start_time = microtime(TRUE);
		$time = abs($start_time - $this->start_time);

		if (@$data['backtrace'])
		{
			$backtrace = $data['backtrace'];
		} else {
			$backtrace = debug_backtrace(FALSE);
		}

		$call = NULL;
		$from = NULL;
		foreach ($backtrace as $index => $step)
		{
			if ($step['class'] != 'ExoTest\\Suite')
			{
				$from = $step;
				$call = @$backtrace[$index - 1];
				break;
			}
		}

		$result = self::RESULT_FAIL;
		if ($bool === TRUE || $bool == self::RESULT_PASS)
		{
			$result = self::RESULT_PASS;
		} elseif ($bool == self::RESULT_SKIP) {
			$result = self::RESULT_SKIP;
		}

		$name = self::get_test_name($this->test_name);
		$method = @$from['function'] ? $from['function'] : @$from['method'];
		$class_method = implode(array(@$from['class'], @$from['type'], $method));
		$line = @$call['line'] - 1;
		$file = self::get_class_filename(@$from['class']);

		$result = array_merge(array(
			'name' => $name,
			'method' => $class_method,
			'line' => $line,
			'result' => $result,
			'description' => @$data['description'],
			'file' => $file,
			'time' => $time,
			'start' => $start_time
		), $data);

		$this->results[] = (object)$result;
	}

	/**
	 * Basic assertion, boolean TRUE
	 * @param bool $bool should be TRUE
	 * @return object result
	 */
	public function assert($bool, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf('Expected TRUE, received %s', ($bool ? 'TRUE' : 'FALSE'));
		}
		$result = $this->add_result($bool, array(
			'description' => $description
		));
		$this->start_time = microtime(TRUE);
		return $result;
	}
	public function assert_true($bool) { return $this->assert($bool); }

	/**
	 * Assert that result is an object
	 * @param mixed $value
	 * @return object result
	 */
	public function assert_object($value, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf('Expected type Object, value is type %s', gettype($value));
		}
		$result = $this->add_result(is_object($value), array(
			'description' => $description
		));
		return $result;
	}
	public function assertObject($value, $description = NULL) { return $this->assert_object($value, $description); }

	/**
	 * Assert that a string contains another string
	 * @param mixed $haystack
	 * @param mixed $needle
	 * @return object result
	 */
	public function assert_contains($haystack, $needle, $description = NULL)
	{
		$haystack_type = gettype($haystack);
		$needle_type = gettype($needle);
		if ($description === NULL) { $description = sprintf('Expected %s to contain %s', ucwords($haystack_type), json_encode($needle)); }

		if ($haystack === NULL && $needle !== NULL) 
		{ 
			return $this->assert(strpos(FALSE, $description));
		}

		if (is_scalar($needle)) { $needle = (string)$needle; }
		if (is_scalar($haystack)) { $haystack = (string)$haystack; }

		if (is_scalar($needle) && is_scalar($haystack))
		{
			return $this->assert(strpos($haystack, $needle) !== FALSE, $description);
		}

		if (is_scalar($haystack) && !is_scalar($needle))
		{
			return $this->assert(FALSE, $description);
		}

		if (is_object($needle)) { $needle = (array)$needle; }
		if (is_object($haystack)) { $haystack = (array)$haystack; }

		if (is_array($haystack) && is_array($needle))
		{
			foreach ($needle as $key => $value)
			{
				if (is_numeric($key) && !in_array($value, $haystack) || @$haystack[$key] != @$value)
				{
					return $this->assert(FALSE, $description);
				}
			}
			return $this->assert(TRUE, $description);
		} elseif (is_scalar($needle) && is_array($haystack)) {
			return $this->assert(in_array($needle, $haystack), $description);
		}
		return $this->assert(FALSE, sprintf('Cannot search for type %s in type %s', 
			$needle_type,
			$haystack_type
		));
	}
	public function assertContains($haystack, $needle, $description = NULL) { return $this->assert_contains($haystack, $needle, $description); }

	/**
	 * Assert that something is empty, entirely empty
	 * @param mixed $input
	 * @return object result
	 */
	public function assert_empty($input, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf('Expected "%s" to be empty', $input);
		}

		return $this->assert(empty($input), $description);
	}
	public function assertEmpty($input, $description = NULL) { return $this->assert_empty($input, $description); }

	/**
	 * Assert the opposite direction
	 * @param bool $result should be FALSE to pass
	 * @return object result
	 */
	public function assert_false($result, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf('Expected FALSE, received %s', ($result ? 'TRUE' : 'FALSE'));
		}

		$result = $result === FALSE;
		return $this->assert($result, $description);
	}
	public function assertFalse($result, $desc = NULL) { return $this->assert_false($result, $desc); }

	public function assert_null($result, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf("Expected NULL, received %s", gettype($result));
		}
		return $this->assert($result === NULL, $description);
	}
	public function assertNull($result, $description = NULL) { return $this->assert_null($result, $description); }

	public function assert_equals($a, $b)
	{
		return $this->assert($a == $b);
	}

	public function display_results()
	{
		$class = 'ExoTest\\Renderer\\' . $this->options['renderer'];
		$renderer = new $class();
		$renderer->suite = $this;
		$output = $renderer->display_results($this->results);
		return $output;
	}

	/**
	 * Rewind (remove) results
	 * @param int $count (optional) defaults to 1
	 * @return bool
	 */
	public function rewind($count = 1)
	{
		$rewinds = array();
		for ($x = 0; $x < $count; $x++)
		{
			$rewinds[] = array_pop($this->results);
		}
		return $count == 1 ? reset($rewinds) : $rewinds;
	}

	/**
	 * Update the time, so that the times are valid
	 */
	public function tick()
	{
		$this->start_time = microtime(TRUE);
	}

	/**
	 * Assert that it is an array
	 */
	public function assert_array($result, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf('Expected Array, received %s', gettype($result));
		}
		return $this->assert(is_array($result), $description);
	}
	public function assertArray($result, $description = NULL) { return $this->assert_array($result, $description); }

	/**
	 * Assert that it is not an array
	 */
	public function assert_not_array($result, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf('Expected non-Array, received %s', gettype($result));
		}
		return $this->assert(!is_array($result), $description);
	}
	public function assertNotArray($result, $description = NULL) { return $this->assert_not_array($result, $description); }

	/**
	 * Assert exception is valid
	 */
	public function assert_exception($e, $test, $description = NULL)
	{
		$reflection = new ReflectionMethod($this, $test);
		$phpdoc = $reflection->getDocComment();
		$split = preg_split("#[\r\n\*\/]+#", $phpdoc);
		$parts = array();
		$exception_type = NULL;
		foreach ($split as $index => $part)
		{
			$part = trim($part);
			$arguments = preg_split('# +#', $part);
			if (@$arguments[0] == '@exception')
			{
				$exception_type = 'Exception';
				if (@$arguments[1])
				{
					$exception_type = $arguments[1];
				}
			}
		}
		if ($description === NULL)
		{
			if ($exception_type !== NULL)
			{
				$description = sprintf('Expected Exception of type %s, received %s with message: %s',
					$exception_type,
					get_class($e),
					$e->getMessage()
				);
			} else {
				$description = sprintf('Unexpected Exception of type %s with message: %s',
					get_class($e),
					$e->getMessage()
				);
			}
		}

		$bool = is_a($e, $exception_type);

		$backtrace = $e->getTrace();
		$step = array(
			'file' => $e->getFile(),
			'line' => $e->getLine(),
			'class' => 'ExoTest\\Suite',
			'method' => $this->test_name
		);
		array_unshift($backtrace, $step);

		$data = array(
			'backtrace' => $backtrace,
			'description' => $description
		);
		$result = $this->add_result(is_a($e, $exception_type), $data);
		$this->tick();
		return $result;
	}

	public function assertFailed($assert, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf('Expected assertion FAIL, received assertion %s', 
				strtoupper($assert->result == self::RESULT_FAIL ? 'fail' : ($assert->result == self::RESULT_PASS ? 'pass' : 'skip'))
			);
		}
		return $this->assert($assert->result == self::RESULT_FAIL, $description);
	}
	public function assert_failed($assert, $description = NULL) { return $this->assertFailed($assert, $description); }

	public function assertPassed($assert, $description = NULL)
	{
		if ($description === NULL)
		{
			$description = sprintf('Expected assertion PASS, received assertion %s', 
				strtoupper($assert->result == self::RESULT_FAIL ? 'fail' : ($assert->result == self::RESULT_PASS ? 'pass' : 'skip'))
			);
		}
		return $this->assert($assert->result == self::RESULT_PASS, $description);
	}
	public function assert_passed($assert, $description = NULL) { return $this->assertPassed($assert, $description); }

	public function assert_fixme($object, $method)
	{
		$reflection = new ReflectionMethod($object, $method);
		$phpdoc = $reflection->getDocComment();
		$split = preg_split("#[\r\n\*\/]+#", $phpdoc);
		$parts = array();
		$exception_type = NULL;
		foreach ($split as $index => $part)
		{
			$part = trim($part);
			$arguments = preg_split('# +#', $part);
			if (@$arguments[0] == '@fixme' || @$arguments[0] == '@todo')
			{
				$this->add_result(self::RESULT_SKIP, array(
					'description' => $arguments[1] === NULL ? $arguments[0] : (implode(' ', array_slice($arguments, 1))),
					'file' => $reflection->getFileName(),
					'line' => $reflection->getStartLine(),
					'class' => get_class($this)
				));
				return TRUE;
			}
		}
		return FALSE;
	}

	public static function __autoload($class)
	{
		$path = __DIR__ . '/../' . str_replace('\\', '/', $class) . '.php';
		if (file_exists($path))
		{
			include($path);
		}
	}

	public function skip($description = NULL)
	{
		if ($description === NULL)
		{
			$description = 'No reason given';
		}
		return $this->add_result(self::RESULT_SKIP, array(
			'description' => $description
		));
	}
}

spl_autoload_register(array('ExoTest\\Suite', '__autoload'));
